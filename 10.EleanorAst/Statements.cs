namespace Eleanor.Ast
{
	public abstract record Statement;

	public sealed record UseStatement(string[] Path) : Statement;

	public sealed record DeclarationStatement : Statement
	{
		public readonly Declaration[] Declarations;
		public readonly Expression Value;

		public DeclarationStatement(Declaration[] declarations, Expression value)
		{
			Declarations = declarations;
			Value = value;
		}

		public override string ToString() => $"let {Declarations.ToStringArrayType()} = {Value}";
	}

	public sealed record DeferStatement : Statement
	{
		public readonly Expression Expression;

		public DeferStatement(Expression expression)
		{
			Expression = expression;
		}

		public override string ToString() => $"defer {Expression}";
	}

	public sealed record BreakStatement
	{
		public readonly Expression Expression;

		public BreakStatement(Expression expression)
		{
			Expression = expression;
		}

		public override string ToString() => $"break {Expression}";
	}

	public sealed record ReturnStatement
	{
		public readonly Expression Expression;

		public ReturnStatement(Expression expression)
		{
			Expression = expression;
		}

		public override string ToString() => $"return {Expression}";
	}

	public sealed record AssignmentStatement : Statement
	{
		public readonly Expression Rhs;
		public readonly Expression Lhs;

		public AssignmentStatement(Expression rhs, Expression lhs)
		{
			Rhs = rhs;
			Lhs = lhs;
		}
	}

	public abstract record ExpressionStatement : Statement
	{
		public readonly Expression Expression;

		public ExpressionStatement(Expression expression)
		{
			Expression = expression;
		}

		public override string ToString() => Expression.ToString()!;
	}

	// public sealed record TupleStatement : ExpressionStatement
	// {
	// 	public readonly ExpressionStatement[] Expressions;

	// 	public TupleStatement(ExpressionStatement[] expressions)
	// 	{
	// 		Expressions = expressions;
	// 	}

	// 	public override string ToString() => $"({Expressions.ToStringArray()})";
	// }

	// public sealed record ValueStatement : ExpressionStatement
	// {
	// 	public readonly IdentifiedOr<Value> Value;

	// 	public ValueStatement(IdentifiedOr<Value> value)
	// 	{
	// 		Value = value;
	// 	}

	// 	public override string ToString() => Value.ToString()!;
	// }

	// public sealed record FunctionCallStatement : ExpressionStatement
	// {
	// 	public readonly IdentifiedOr<Function> Function;
	// 	public readonly ExpressionStatement[] Parameters;

	// 	public FunctionCallStatement
	// 	(
	// 		IdentifiedOr<Function> function,
	// 		ExpressionStatement[] parameters
	// 	)
	// 	{
	// 		Function = function;
	// 		Parameters = parameters;
	// 	}

	// 	public override string ToString() => $"{Function}({Parameters.ToStringArray()})";
	// }

	// public enum Operators
	// {
	// 	Plus, Minus, Mul, Div,
	// 	Equal, NotEqual,
	// 	SelectField, SelectIndex,
	// 	Ref, Deref,
	// 	Cast,
	// 	Initialize, Assign,
	// 	Function,
	// 	SubModule,
	// }

	// public sealed record OperatorStatement : ExpressionStatement
	// {
	// 	public readonly ExpressionStatement Lhs;
	// 	public readonly ExpressionStatement Rhs;
	// 	public readonly Operators Operator;

	// 	public OperatorStatement(ExpressionStatement lhs, ExpressionStatement rhs, Operators @operator)
	// 	{
	// 		Lhs = lhs;
	// 		Rhs = rhs;
	// 		Operator = @operator;
	// 	}

	// 	public override string ToString() => $"{Lhs} {Operator} {Rhs}";
	// }
}
