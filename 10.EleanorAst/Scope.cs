namespace Eleanor.Ast
{
	public class Scope
	{
		public readonly Statement[] Statements;

		public Scope(Statement[] statements)
		{
			Statements = statements;
		}

		public override string ToString() => Statements.ToStringArrayScope(true);
	}
}
