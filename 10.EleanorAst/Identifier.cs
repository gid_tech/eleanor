namespace Eleanor.Ast
{
	public readonly record struct Identifier(string Value)
	{
		public override string ToString() => Value;
	}

	// public readonly struct Identified
	// {
	// 	public readonly Identifier? Identifier;
	// 	public readonly Expression? Value;

	// 	public Identified(Identifier identifier)
	// 	{
	// 		Identifier = identifier;
	// 		Value = null;
	// 	}

	// 	public Identified(Expression value)
	// 	{
	// 		Identifier = null;
	// 		Value = value;
	// 	}
	// }
}
