namespace Eleanor.Ast
{
	public class Declaration
	{
		public readonly bool IsMut;
		public readonly Identifier Identifier;
		public readonly Expression Type;

		public Declaration(bool isMut, Identifier identifier, Expression type)
		{
			IsMut = isMut;
			Identifier = identifier;
			Type = type;
		}

		public override string ToString() => (IsMut ? "mut " : "") + $"{Identifier} {Type}";
	}

	// public class DefDeclaration : Declaration
	// {
	// 	public DefDeclaration(Identifier identifier, IdentifiedOr<Type> type)
	// 	: base(identifier, type)
	// 	{
	// 	}

	// 	public override string ToString() => $"{Identifier} {Type}";
	// }

	// public class GenericDeclaration : Declaration
	// {
	// 	public GenericDeclaration(Identifier identifier, IdentifiedOr<Type> type)
	// 	: base(identifier, type)
	// 	{
	// 	}

	// 	public override string ToString() => $"{Identifier} {Type}";
	// }

	// public class MutDeclaration : Declaration
	// {
	// 	public readonly bool IsMut;

	// 	public MutDeclaration(Identifier identifier, IdentifiedOr<Type> type, bool isMut)
	// 	: base(identifier, type)
	// 	{
	// 		IsMut = isMut;
	// 	}

	// 	public override string ToString() => IsMut ? "mut " + base.ToString() : base.ToString();
	// }

	// public class LetDeclaration : MutDeclaration
	// {
	// 	public LetDeclaration(Identifier identifier, IdentifiedOr<Type> type, bool isMut)
	// 	: base(identifier, type, isMut)
	// 	{
	// 	}
	// }

	// public class ParameterDeclaration : MutDeclaration
	// {
	// 	public ParameterDeclaration(Identifier identifier, IdentifiedOr<Type> type, bool isMut)
	// 	: base(identifier, type, isMut)
	// 	{
	// 	}
	// }
}
