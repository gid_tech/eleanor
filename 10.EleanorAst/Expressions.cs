namespace Eleanor.Ast
{
	public abstract class Expression
	{
	}

	public class MultiExpression : Expression
	{
		public readonly Expression[] Expressions;

		public MultiExpression(Expression[] expressions)
		{
			Expressions = expressions;
		}

		public override string ToString() => Expressions.ToStringArrayExpression();
	}

	public class IdentifiedExpression : Expression
	{
		public readonly Identifier Identifier;

		public IdentifiedExpression(Identifier identifier)
		{
			Identifier = identifier;
		}

		public override string ToString() => Identifier.ToString();
	}

	public class ValueExpression : Expression
	{
		public readonly Value Value;

		public ValueExpression(Value value)
		{
			Value = value;
		}

		public override string ToString() => Value.ToString()!;
	}

	public class CallExpression : Expression
	{
		public readonly Expression Function;
		public readonly Expression[] Parameters;

		public CallExpression(Expression function, Expression[] parameters)
		{
			Function = function;
			Parameters = parameters;
		}

		public override string ToString() => $"{Function}({Parameters.ToStringArray()})";
	}

	public class SelectExpression : Expression
	{
		public readonly Identifier Identifier;

		public SelectExpression(Identifier identifier)
		{
			Identifier = identifier;
		}

		public override string ToString() => $".{Identifier}";
	}

	public class OperatorExpression : Expression
	{
		public readonly Expression Lhs;
		public readonly Expression Rhs;
		public readonly Operators Operator;

		public OperatorExpression(Expression lhs, Expression rhs, Operators @operator)
		{
			Lhs = lhs;
			Rhs = rhs;
			Operator = @operator;
		}

		public override string ToString() => $"{Lhs} {Operator} {Rhs}";
	}

	public enum Operators
	{
		Plus, Minus, Mul, Div,
		Equal, NotEqual,
		SelectField, SelectIndex,
		Ref, Deref,
		Cast,
		Initialize, Assign,
		Function,
		SubModule,
	}

	public readonly struct If
	{
		public readonly Expression Condition;
		public readonly Scope Scope;
	}

	public readonly struct ElseIf
	{
		public readonly Expression Condition;
		public readonly Scope Scope;
	}

	public readonly struct Else
	{
		public readonly Scope Scope;
	}

	public class ConditionExpression : Expression
	{
		public readonly If If;
		public readonly ElseIf[]? ElseIfs;
		public readonly Else? Else;

		public ConditionExpression(If @if, ElseIf[]? elseIfs, Else? @else)
		{
			If = @if;
			ElseIfs = elseIfs;
			Else = @else;
		}
	}

	public readonly struct While
	{
		public readonly Expression Condition;
		public readonly Expression Scope;
	}

	public class LoopExpression : Expression
	{
		public readonly While While;
		public readonly Else? Else;

		public LoopExpression(While @while, Else? @else)
		{
			While = @while;
			Else = @else;
		}
	}

	public class ScopeExpression : Expression
	{
		public readonly Statement[] Statements;

		public ScopeExpression(Statement[] statements)
		{
			Statements = statements;
		}

		// public override string ToString() => $"{{\n{Statements.ToStringArray(true)}\n\t}}";
	}
}
