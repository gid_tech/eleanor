namespace Eleanor.Ast
{
	public abstract class Type
	{
		public virtual ulong Size { get; }
		public virtual ulong Alignment { get; }
	}

	public class ModuleType : Type
	{
		public override string ToString() => "module";
	}

	public class TypeType : Type
	{
		public override string ToString() => "type";
	}

	public class FuncType : Type
	{
		public readonly Declaration[] Inputs;
		public readonly Expression[] Outputs;
		public readonly string? Convention;

		public FuncType(Declaration[] inputs, Expression[] outputs, string? convention)
		{
			Inputs = inputs;
			Outputs = outputs;
			Convention = convention;
		}

		public override string ToString() => $"func {Inputs.ToStringArrayType()} {Outputs.ToStringArrayType()} {Convention?.ToStringString()}";
	}

	public abstract class ScalarType : Type
	{
	}

	public abstract class ElementaryType : ScalarType
	{
		public sealed override ulong Alignment => Size;
	}

	public class BoolType : ElementaryType
	{
		public override ulong Size => 1;
		public override string ToString() => "bool";
	}

	public class CharType : ElementaryType
	{
		public override ulong Size => 1;
		public override string ToString() => "char";
	}

	public class ByteType : ElementaryType
	{
		public override ulong Size => 1;
		public override string ToString() => "byte";
	}

	public class ShortType : ElementaryType
	{
		public override ulong Size => 2;
		public override string ToString() => "short";
	}

	public class IntType : ElementaryType
	{
		public override ulong Size => 4;
		public override string ToString() => "int";
	}

	public class LongType : ElementaryType
	{
		public override ulong Size => 8;
		public override string ToString() => "long";
	}

	public class UByteType : ElementaryType
	{
		public override ulong Size => 1;
		public override string ToString() => "ubyte";
	}

	public class UShortType : ElementaryType
	{
		public override ulong Size => 2;
		public override string ToString() => "ushort";
	}

	public class UIntType : ElementaryType
	{
		public override ulong Size => 4;
		public override string ToString() => "uint";
	}

	public class ULongType : ElementaryType
	{
		public override ulong Size => 8;
		public override string ToString() => "ulong";
	}

	public class FloatType : ElementaryType
	{
		public override ulong Size => 4;
		public override string ToString() => "float";
	}

	public class DoubleType : ElementaryType
	{
		public override ulong Size => 8;
		public override string ToString() => "double";
	}

	public abstract class PtrType : Type
	{
		public readonly Expression Type;

		protected PtrType(Expression type)
		{
			Type = type;
		}
	}

	public class RefType : PtrType
	{
		public RefType(Expression type) : base(type)
		{
		}

		public override string ToString() => $"ref {Type}";
	}

	public class ArrayType : PtrType
	{
		public ArrayType(Expression type) : base(type)
		{
		}

		public override string ToString() => $"array {Type}";
	}

	public class UnionType : ScalarType
	{
		public readonly Field[] Fields;

		public override ulong Size { get; }
		public override ulong Alignment { get; }

		public UnionType(Field[] fields)
		{
			Fields = fields;
		}

		public override string ToString() => $"union[{string.Join(", ", Fields)}]";
	}

	public class EnumType : ScalarType
	{
		public readonly Expression Type;
		public readonly EnumElement[] Elements;

		public EnumType(Expression type, EnumElement[] elements)
		{
			Type = type;
			Elements = elements;
		}

		public override string ToString() => $"enum {Type} [{string.Join(", ", Elements)}]";
	}

	public abstract class AggregateType : Type
	{
	}

	public class BlockType : AggregateType
	{
		public readonly Expression Value;
		public readonly Expression Type;

		public BlockType(Expression value, Expression type)
		{
			Value = value;
			Type = type;
		}

		public override string ToString() => $"block[{Value} {Type}]";
	}

	public class RecordType : AggregateType
	{
		public readonly Field[] Fields;

		public RecordType(Field[] fields)
		{
			Fields = fields;
		}

		public override string ToString() => $"record[{string.Join(", ", Fields)}]";
	}

	public class StrType : Type
	{
	}

	// public class SliceType : FatType
	// {
	// 	public readonly Type Type;

	// 	public SliceType(Type type)
	// 	{
	// 		Type = type;
	// 	}
	// }

	// public class TypeInfoType : FatType
	// {
	// }

	// public class FuncInfoType : FatType
	// {
	// }

	public readonly struct Field
	{
		public readonly Identifier Identifier;
		public readonly Expression Type;

		public Field(Identifier identifier, Expression type)
		{
			Identifier = identifier;
			Type = type;
		}

		public override string ToString() => $"{Identifier} {Type}";
	}

	public readonly struct EnumElement
	{
		public readonly Identifier Identifier;
		public readonly Expression Value;

		public EnumElement(Identifier identifier, Expression value)
		{
			Identifier = identifier;
			Value = value;
		}

		public override string ToString() => $"{Identifier} {Value}";
	}
}
