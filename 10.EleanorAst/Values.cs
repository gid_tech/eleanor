namespace Eleanor.Ast
{
	public abstract record Value;
	public sealed record DefaultValue : Value;
	public sealed record UninitializedValue : Value;
	public sealed record BoolValue(bool Value) : Value;
	public sealed record IntValue(long Value) : Value;
	public sealed record UIntValue(ulong Value) : Value;
	public sealed record FloatValue(double Value) : Value;
	public sealed record StringValue(string Value) : Value;
	public sealed record ArrayValue(Expression[] Value) : Value;
	public sealed record AggregateValue((string, Expression)[] Value) : Value;
}
