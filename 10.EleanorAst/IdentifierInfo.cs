using System.Collections.Generic;

namespace Eleanor.Ast
{
	// public readonly struct Info<T>
	// {
	// 	public readonly T Value;
	// 	public readonly Statement Statement;
	// }

	// public class ScopeIdentifierInfo
	// {
	// 	private readonly Dictionary<Identified<Type>, Info<Type>> Types = new(250);
	// 	private readonly Dictionary<Identified<Value>, Info<Value>> Values = new(250);
	// 	private readonly Dictionary<Identified<Function>, Info<Function>> Functions = new(250);
	// 	private readonly Dictionary<Identified<Generic>, Info<Generic>> Generics = new(250);

	// 	public void Add(Identified<Type> identified, Info<Type> info) => Types.Add(identified, info);
	// 	public void Add(Identified<Value> identified, Info<Value> info) => Values.Add(identified, info);
	// 	public void Add(Identified<Function> identified, Info<Function> info) => Functions.Add(identified, info);
	// 	public void Add(Identified<Generic> identified, Info<Generic> info) => Generics.Add(identified, info);

	// 	public Info<Type> Get(Identified<Type> identified) => Types[identified];
	// 	public Info<Value> Get(Identified<Value> identified) => Values[identified];
	// 	public Info<Function> Get(Identified<Function> identified) => Functions[identified];
	// 	public Info<Generic> Get(Identified<Generic> identified) => Generics[identified];
	// }

	// public class IdentifierInfo
	// {
	// 	private readonly Dictionary<(Scope, (Identifier, Generic)[]), ScopeIdentifierInfo> ScopeInfos = new(100);
	// }
}
