namespace Eleanor.Ast
{
	// public readonly struct Signature
	// {
	// 	public readonly Declaration[] Inputs;
	// 	public readonly Declaration[] Outputs;

	// 	public Signature(Declaration[] inputs, Declaration[] outputs)
	// 	{
	// 		Inputs = inputs;
	// 		Outputs = outputs;
	// 	}

	// 	public override string ToString() => $"({Inputs.ToStringArray()}) ({Outputs.ToStringArray()})";
	// }

	// public abstract class Function
	// {
	// 	public readonly Signature Signature;

	// 	public Function(Signature signature)
	// 	{
	// 		Signature = signature;
	// 	}

	// 	public override string ToString() => Signature.ToString();
	// }

	// public class ExternFunction : Function
	// {
	// 	public readonly string Convention;
	// 	public readonly string ImportName;

	// 	public ExternFunction(Signature signature, string convention, string importName)
	// 	: base(signature)
	// 	{
	// 		Convention = convention;
	// 		ImportName = importName;
	// 	}

	// 	public override string ToString() => base.ToString() + $" extern {Convention.ToStringString()} {ImportName.ToStringString()}";
	// }

	// public class ImplementedFunction : Function
	// {
	// 	public readonly Scope Scope;

	// 	public ImplementedFunction(Signature signature, Scope scope)
	// 	: base(signature)
	// 	{
	// 		Scope = scope;
	// 	}

	// 	public override string ToString() => base.ToString() + $" {Scope}";
	// }
}
