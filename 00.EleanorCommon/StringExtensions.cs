using System.Text;

namespace Eleanor.Common
{
	public static class StringExtensions
	{
		public static string Escape(this string s)
		{
			var ret = new StringBuilder();
			// ret.reserve(s.size());
			foreach (var c in s)
			{
				switch (c)
				{
					case '\n': ret.Append("\\n"); break;
					case '\r': ret.Append("\\r"); break;
					case '\t': ret.Append("\\t"); break;
					case '\"': ret.Append("\\\""); break;
					case '\\': ret.Append("\\\\"); break;
					default: ret.Append(c); break;
				}
			}
			return ret.ToString();
		}

		public static string Unescape(this string s)
		{
			var ret = new StringBuilder();
			var isEscaped = false;
			foreach (var c in s)
			{
				if (isEscaped)
				{
					isEscaped = false;
					switch (c)
					{
						case 'n': ret.Append('\n'); break;
						case 'r': ret.Append('\r'); break;
						case 't': ret.Append('\t'); break;
						case '"': ret.Append('\"'); break;
						case '\\': ret.Append('\\'); break;
						default: ret.Append(c); break;
					}
				}
				else if (c == '\\')
				{
					isEscaped = true;
				}
				else
				{
					ret.Append(c);
				}
			}
			return ret.ToString();
		}
	}
}
