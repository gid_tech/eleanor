using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Eleanor.Common
{
	public class TempList<T> : List<T>, IDisposable
	{
		private readonly static ConcurrentStack<TempList<T>> ListCache = new();

		private TempList() : base() { }
		private TempList(int capacity) : base(capacity) { }
		private TempList(IEnumerable<T> collection) : base(collection) { }

		public static TempList<T> Get()
		{
			if (ListCache.TryPop(out var list))
				return list;
			else
				return new TempList<T>();
		}

		public static TempList<T> Get(int capacity)
		{
			if (ListCache.TryPop(out var list))
			{
				if (list.Capacity < capacity)
					list.Capacity = capacity;
				return list;
			}
			else
				return new TempList<T>(capacity);
		}

		public static TempList<T> Get(IEnumerable<T> collection)
		{
			if (ListCache.TryPop(out var list))
			{
				list.AddRange(collection);
				return list;
			}
			else
				return new TempList<T>(collection);
		}

		public void Dispose()
		{
			Clear();
			ListCache.Push(this);
			GC.SuppressFinalize(this);
		}
	}
}
