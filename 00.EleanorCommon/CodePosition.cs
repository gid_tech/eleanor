namespace Eleanor.Common
{
	public readonly record struct CodePosition
	{
		public readonly string File;
		public readonly int Line;
		public readonly int Column;

		public CodePosition(string file)
		{
			File = file;
			Line = 1;
			Column = 1;
		}

		private CodePosition(string file, int line, int column)
		{
			File = file;
			Line = line;
			Column = column;
		}

		public CodePosition IncrementLine(int lines = 1) => new(File, Line + lines, 1);
		public CodePosition IncrementColum(int columns = 1) => new(File, Line, Column + columns);

		public override string ToString() => $"{File}({Line},{Column})";
	}
}
