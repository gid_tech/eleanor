namespace Eleanor.Ast
{
	public static class ToStringExtensions
	{
		public static string ToStringArray<T>(this T[] array, bool newline = false) => string.Join(newline ? "\n" : ", ", array);

		public static string ToStringArrayExpression<T>(this T[] array, bool newline = false) => $"({(newline ? "\n" : "")}{array.ToStringArray(newline)}{(newline ? "\n" : "")})";
		public static string ToStringArrayType<T>(this T[] array, bool newline = false) => $"[{(newline ? "\n" : "")}{array.ToStringArray(newline)}{(newline ? "\n" : "")}]";
		public static string ToStringArrayScope<T>(this T[] array, bool newline = false) => $"{{{(newline ? "\n" : "")}{array.ToStringArray(newline)}{(newline ? "\n" : "")}}}";

		public static string ToStringString(this string s) => $"\"{s}\"";
	}
}
