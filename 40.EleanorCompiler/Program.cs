﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Eleanor.Parsing;

namespace Eleanor.Compiler
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;
			var path = Path.GetFullPath("tests/min.ela");
			var source = File.ReadAllText(path);

			var lexSuccess = Lexer.Lex(source, path, out var lexResult, true);
			var parseSuccess = Parser.Parse(lexResult.Tokens, out var parseResult, true);

			Console.WriteLine(lexResult.ToString());
			Console.WriteLine();
			Console.WriteLine(parseResult.ToString());
			Console.WriteLine();

			// Profile(source, path);

			return;
		}

		public static void Profile(string source, string path)
		{
			void F(int i)
			{
				var lexSuccess = Lexer.Lex(source, path, out var lexResult, true);
				var parseSuccess = Parser.Parse(lexResult.Tokens, out var parseResult, true);
			}

			const int count = 100000;

			var start = DateTime.Now;
			Parallel.For(0, count, F);
			var end = DateTime.Now;

			Console.WriteLine($"single: {(end - start) / count}");
			Console.WriteLine($"total: {end - start}");
		}
	}
}
