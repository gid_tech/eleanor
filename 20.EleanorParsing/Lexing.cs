﻿using System;
using System.Globalization;
using System.Linq;
using Eleanor.Ast;
using Eleanor.Common;

namespace Eleanor.Parsing
{
	public static class Lexer
	{
		private static readonly (string Str, KeyWords KeyWord)[] KeyWordMap =
			Enum.GetValues<KeyWords>().Select(k => k.ToNameMap()).ToArray();

		private static readonly (string Str, BaseTypes BaseType)[] BaseTypeMap =
			Enum.GetValues<BaseTypes>().Select(t => t.ToNameMap()).ToArray();

		private static readonly (string Str, Operators Operator)[] OperatorMap = new (string, Operators)[] {
			("==", Operators.Equal),
			("!=", Operators.NotEqual),
			(".", Operators.SelectField),
			("#", Operators.SelectIndex),
			("?", Operators.Ref),
			("!", Operators.Deref),
			("~", Operators.Cast),
			("=", Operators.Initialize),
			("<-", Operators.Assign),
			("->", Operators.Function),
			("+", Operators.Plus),
			("-", Operators.Minus),
			("*", Operators.Mul),
			("/", Operators.Div),
			(":", Operators.SubModule),
		};

		public static bool Lex(ReadOnlySpan<char> source, string file, out LexResult lexResult, bool skipErrors = false)
		{
			using var tokens = TempList<Token>.Get(source.Length / 3);
			using var errors = TempList<LexError>.Get(0);
			var position = new CodePosition(file);

			while (source.Length > 0)
			{
				var first = source[0];
				if (first is '\n')
				{
					source = source[1..];
					position = position.IncrementLine();
				}
				else if (char.IsWhiteSpace(first) || first == ',')
				{
					source = source[1..];
					position = position.IncrementColum();
				}
				else if (source.StartsWith("//"))
				{
					source = source[2..];
					var index = source.IndexOf('\n');
					source = index > 0 ? source[(index + 1)..] : "";
					position = position.IncrementLine();
				}
				else if (source.StartsWith("/*"))
				{
					source = source[2..];
					position = position.IncrementColum(2);
					var commentDepth = 1;

					while (commentDepth > 0 && source.Length > 0)
					{
						if (source.StartsWith("/*"))
						{
							source = source[2..];
							position = position.IncrementColum(2);
							commentDepth += 1;
						}
						else if (source.StartsWith("*/"))
						{
							source = source[2..];
							position = position.IncrementColum(2);
							commentDepth -= 1;
						}
						else
						{
							source = source[1..];
							position = source.StartsWith("\n")
								? position.IncrementLine()
								: position.IncrementColum();
						}
					}
				}
				else if (LexToken(source, position, out var tokenResult))
				{
					tokens.Add(tokenResult.Token);
					source = tokenResult.Source;
					position = tokenResult.Position;
				}
				else if (skipErrors)
				{
					errors.Add(new LexError(position));
					source = source[1..];
					position = position.IncrementColum();
				}
				else
				{
					break;
				}
			}

			lexResult = new LexResult(tokens.ToArray(), errors.ToArray());
			return errors.Count == 0;
		}

		private static bool LexToken(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (LexBool(source, position, out result)) return true;
			if (LexNumber(source, position, out result)) return true;
			if (LexChar(source, position, out result)) return true;
			if (LexString(source, position, out result)) return true;
			if (LexBracket(source, position, out result)) return true;
			if (LexOperator(source, position, out result)) return true;
			if (LexKeyWord(source, position, out result)) return true;
			if (LexBaseType(source, position, out result)) return true;
			if (LexIdentifier(source, position, out result)) return true;
			return false;
		}

		private static bool LexBool(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			const string True = "true";
			const string False = "false";

			static Result MakeBool(bool value, ReadOnlySpan<char> source, in CodePosition position)
			{
				var length = value ? True.Length : False.Length;
				return new Result(
					new Token(TokenKinds.Bool, new TokenValue { Bool = value }, position),
					source[length..],
					position.IncrementColum(length)
				);
			}

			if (IsWordExactly(source, True))
			{
				result = MakeBool(true, source, position);
				return true;
			}
			else if (IsWordExactly(source, False))
			{
				result = MakeBool(false, source, position);
				return true;
			}

			result = default;
			return false;
		}

		private static bool LexNumber(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (source[0] is var first && Char.IsDigit(first) || first == '-')
			{
				var length = 1;
				foreach (var c in source[1..])
				{
					if (Char.IsDigit(c) || "abcdefABCDEF".Contains(c) || c is '.' or 'e' or 'x' or '-')
						length += 1;
					else
						break;
				}

				var numberSource = source[..length];

				if (
					numberSource.StartsWith("0x")
					&& Int64.TryParse(numberSource[2..], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out var hexiv))
				{
					result = new Result(
						new Token(
							TokenKinds.Int, new TokenValue { Int = hexiv }, position
						),
						source[length..],
						position.IncrementColum(length)
					);
					return true;
				}
				else if (
					(Char.IsDigit(first) || (first is '-' && numberSource.Length >= 2))
					&& (numberSource.Contains('.') || numberSource.Contains('e'))
					&& Double.TryParse(numberSource, NumberStyles.Float, CultureInfo.InvariantCulture, out var dv))
				{
					result = new Result(
						new Token(
							TokenKinds.Float, new TokenValue { Float = dv }, position
						),
						source[length..],
						position.IncrementColum(length)
					);
					return true;
				}
				else if (
					first is '-'
					&& numberSource.Length >= 2
					&& Int64.TryParse(numberSource, NumberStyles.Integer | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out var iv))
				{
					result = new Result(
						new Token(
							TokenKinds.Int, new TokenValue { Int = iv }, position
						),
						source[length..],
						position.IncrementColum(length)
					);
					return true;
				}
				else if (
					Char.IsDigit(first) &&
					UInt64.TryParse(numberSource, NumberStyles.Integer, CultureInfo.InvariantCulture, out var uiv))
				{
					result = new Result(
						new Token(
							TokenKinds.UInt, new TokenValue { UInt = uiv }, position
						),
						source[length..],
						position.IncrementColum(length)
					);
					return true;
				}
			}

			result = default;
			return false;
		}

		private static bool LexChar(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (source.Length >= 3 && source[0] is '\'' && source[2] is '\'')
			{
				result = new Result(
					new Token(TokenKinds.Char, new TokenValue { Char = source[1] }, position),
					source[3..],
					position.IncrementColum(3)
				);
				return true;
			}
			else if (
				source.Length >= 4 && source[0] is '\'' && source[1] is '\\' && source[3] is '\''
				&& source[1..3].ToString().Unescape() is var str && str.Length == 1)
			{
				result = new Result(
					new Token(TokenKinds.Char, new TokenValue { Char = str[0] }, position),
					source[4..],
					position.IncrementColum(4)
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool LexString(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (source.Length >= 2 && source[0] == '"')
			{
				var escaped = false;
				var length = 1;
				var endFound = false;
				foreach (var c in source[1..])
				{
					if (!escaped && c == '"')
						endFound = true;
					else if (c == '\\')
						escaped = true;
					else
						escaped = false;
					length += 1;
					if (endFound)
						break;
				}

				if (endFound)
				{
					result = new Result(
						new Token(
							TokenKinds.String,
							new TokenValue { String = source[1..(length - 1)].ToString().Unescape() },
							position
						),
					 	source[length..],
						position.IncrementColum(length)
					);
					return true;
				}
			}

			result = default;
			return false;
		}

		private static bool LexBracket(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			static Result MakeBracket(Brackets bracket, ReadOnlySpan<char> source, in CodePosition position) => new Result(
				new Token(TokenKinds.Bracket, new TokenValue { Bracket = bracket }, position),
				source[1..],
				position.IncrementColum()
			);

			switch (source[0])
			{
				case '(':
					result = MakeBracket(Brackets.OpenExpression, source, position);
					return true;
				case ')':
					result = MakeBracket(Brackets.CloseExpression, source, position);
					return true;
				case '[':
					result = MakeBracket(Brackets.OpenType, source, position);
					return true;
				case ']':
					result = MakeBracket(Brackets.CloseType, source, position);
					return true;
				case '{':
					result = MakeBracket(Brackets.OpenScope, source, position);
					return true;
				case '}':
					result = MakeBracket(Brackets.CloseScope, source, position);
					return true;
				default: break;
			}

			result = default;
			return false;
		}

		private static bool LexOperator(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (
				FindMapIndexOperator(OperatorMap, source) is var i
				&& i >= 0)
			{
				var (str, @operator) = OperatorMap[i];
				result = new Result(
					new Token(TokenKinds.Operator, new TokenValue { Operator = @operator }, position),
					source[str.Length..],
					position.IncrementColum(str.Length)
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool LexKeyWord(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			var first = source[0];
			if (
				Char.IsLetter(first)
				&& FindMapIndexWord(KeyWordMap, source) is var i
				&& i >= 0)
			{
				var (str, keyWord) = KeyWordMap[i];
				result = new Result(
					new Token(TokenKinds.KeyWord, new TokenValue { KeyWord = keyWord }, position),
					source[str.Length..],
					position.IncrementColum(str.Length)
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool LexBaseType(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			var first = source[0];
			if (
				Char.IsLetter(first)
				&& FindMapIndexWord(BaseTypeMap, source) is var i
				&& i >= 0)
			{
				var (str, baseType) = BaseTypeMap[i];
				result = new Result(
					new Token(TokenKinds.BaseType, new TokenValue { BaseType = baseType }, position),
					source[str.Length..],
					position.IncrementColum(str.Length)
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool LexIdentifier(ReadOnlySpan<char> source, in CodePosition position, out Result result)
		{
			if (Char.IsLetter(source[0]))
			{
				var length = 1;
				foreach (var c in source[1..])
				{
					if (Char.IsLetterOrDigit(c))
						length += 1;
					else
						break;
				}

				result = new Result(
					new Token(TokenKinds.Identifier, new TokenValue { Identifier = source[..length].ToString() }, position),
					source[length..],
					position.IncrementColum(length)
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool IsWordExactly(ReadOnlySpan<char> source, string s)
		{
			if (source.StartsWith(s))
			{
				if (source.Length > s.Length)
				{
					var nextC = source[s.Length];
					if (!Char.IsLetter(nextC) && !Char.IsNumber(nextC))
						return true;
				}
				else
					return true;
			}
			return false;
		}

		private static bool IsOperatorExactly(ReadOnlySpan<char> source, string s)
		{
			if (source.StartsWith(s))
			{
				if (source.Length > s.Length)
				{
					var nextC = source[s.Length];
					if (!"+-*/<>.~!?#%&".Contains(nextC))
						return true;
				}
				else
					return true;
			}
			return false;
		}

		private static int FindMapIndexWord<T>((string Str, T Val)[] map, ReadOnlySpan<char> source)
		{
			foreach (var ((str, _), i) in map.Select((v, i) => (v, i)))
			{
				if (IsWordExactly(source, str))
					return i;
			}
			return -1;
		}

		private static int FindMapIndexOperator<T>((string Str, T Val)[] map, ReadOnlySpan<char> source)
		{
			foreach (var ((str, _), i) in map.Select((v, i) => (v, i)))
			{
				if (IsOperatorExactly(source, str))
					return i;
			}
			return -1;
		}

		private readonly ref struct Result
		{
			public readonly Token Token;
			public readonly ReadOnlySpan<char> Source;
			public readonly CodePosition Position;

			public Result(Token token, ReadOnlySpan<char> source, in CodePosition position)
			{
				Token = token;
				Source = source;
				Position = position;
			}
		}
	}

	public readonly struct LexResult
	{
		public readonly Token[] Tokens;
		public readonly LexError[] Errors;

		public LexResult(Token[] tokens, LexError[] errors)
		{
			Tokens = tokens;
			Errors = errors;
		}

		public override string ToString()
		{
			var str = string.Empty;
			if (Tokens.Length > 0)
				str += $"Tokens: {Tokens.ToStringArrayScope(true)}";
			if (Errors.Length > 0)
				str += $"\nLexErrors: {Errors.ToStringArrayExpression(true)}";
			return str;
		}
	}

	public readonly struct LexError
	{
		public readonly CodePosition Position;

		public LexError(CodePosition position)
		{
			Position = position;
		}

		public override string ToString() => Position.ToString();
	}

	internal static class Extenseions
	{
		public static (string, KeyWords) ToNameMap(this KeyWords keyWord) =>
			(keyWord.ToString().ToLower(), keyWord);

		public static (string, BaseTypes) ToNameMap(this BaseTypes baseTypes) =>
			(baseTypes.ToString().ToLower(), baseTypes);
	}
}
