using System.Runtime.InteropServices;
using Eleanor.Ast;
using Eleanor.Common;

namespace Eleanor.Parsing
{
	public enum TokenKinds
	{
		Bool,
		Int,
		UInt,
		Float,
		Char,
		String,
		Bracket,
		Operator,
		KeyWord,
		BaseType,
		Identifier,
	}

	public enum Brackets
	{
		OpenExpression, CloseExpression,
		OpenScope, CloseScope,
		OpenType, CloseType,
	}

	public enum Operators
	{
		Plus, Minus, Mul, Div,
		Equal, NotEqual,
		SelectField, SelectIndex,
		Ref, Deref,
		Cast,
		Initialize, Assign,
		Function,
		SubModule,
	}

	public enum KeyWords
	{
		Use,
		Def, Let, Mut,
		If, While, Defer,
		Return, Break,
		Extern,
	}

	public enum BaseTypes
	{
		Module, Type,
		Bool, Char,
		Byte, Short, Int, Long,
		UByte, UShort, UInt, ULong,
		Float, Double,
		Ref, Array, Func,
		Str, Slice, Range, Typeinfo, Funcinfo,
		Block, Record, Union, Enum
	}

	[StructLayout(LayoutKind.Explicit)]
	public struct TokenValue
	{
		[FieldOffset(0)]
		public bool Bool;
		[FieldOffset(0)]
		public long Int;
		[FieldOffset(0)]
		public ulong UInt;
		[FieldOffset(0)]
		public double Float;
		[FieldOffset(0)]
		public char Char;
		[FieldOffset(sizeof(long))]
		public string String;
		[FieldOffset(0)]
		public Brackets Bracket;
		[FieldOffset(0)]
		public Operators Operator;
		[FieldOffset(0)]
		public KeyWords KeyWord;
		[FieldOffset(0)]
		public BaseTypes BaseType;
		[FieldOffset(sizeof(long))]
		public string Identifier;
	}

	public readonly struct Token
	{
		public readonly TokenKinds Kind;
		public readonly TokenValue Value;
		public readonly CodePosition Position;

		public Token(TokenKinds kind, TokenValue value, CodePosition position)
		{
			Kind = kind;
			Value = value;
			Position = position;
		}

		public override string ToString()
		{
			var value = Kind switch
			{
				TokenKinds.Bool => $"{Value.Bool}",
				TokenKinds.Int => $"{Value.Int}",
				TokenKinds.UInt => $"{Value.UInt}",
				TokenKinds.Float => $"{Value.Float}",
				TokenKinds.Char => $"'{Value.Char}'".Escape(),
				TokenKinds.String => $"\"{Value.String.Escape()}\"",
				TokenKinds.Bracket => $"{Value.Bracket}",
				TokenKinds.Operator => $"{Value.Operator}",
				TokenKinds.KeyWord => $"{Value.KeyWord}",
				TokenKinds.BaseType => $"{Value.BaseType}",
				TokenKinds.Identifier => $"{Value.Identifier}",
				_ => throw new System.Exception(),
			};
			return $"{Kind}: {value} at {Position}";
		}
	}
}
