﻿using System;
using Eleanor.Ast;
using Eleanor.Common;
using Type = Eleanor.Ast.Type;

namespace Eleanor.Parsing
{
	public static class Parser
	{
		public static bool Parse(ReadOnlySpan<Token> tokens, out ParseResult parseResult, bool skipErrors = false)
		{
			using var statements = TempList<Statement>.Get(tokens.Length / 8);
			using var errors = TempList<ParseError>.Get(0);

			while (tokens.Length > 0)
			{
				if (ParseStatement(tokens, out var statementResult))
				{
					statements.Add(statementResult.Value);
					tokens = statementResult.Tokens;
				}
				else if (skipErrors)
				{
					errors.Add(new ParseError(tokens[0]));
					tokens = tokens[1..];
				}
				else
				{
					break;
				}
			}

			parseResult = new ParseResult(new Scope(statements.ToArray()), errors.ToArray());
			return errors.Count == 0;
		}

		private static bool ParseStatement(ReadOnlySpan<Token> tokens, out Result<Statement> result)
		{
			if (ParseUseStatement(tokens, out result)) return true;
			if (ParseDeclarationStatement(tokens, out result)) return true;
			return false;
		}

		#region statements

		private static bool ParseUseStatement(ReadOnlySpan<Token> tokens, out Result<Statement> result)
		{
			if (
				tokens.Length >= 1
				&& tokens[0] is { Kind: TokenKinds.KeyWord, Value: { KeyWord: KeyWords.Use } }
				&& ParseExpression(tokens[1..], out var expressionResult)
			)
			{
				result = new Result<Statement>(
					new UseStatement(expressionResult.Value),
					expressionResult.Tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		private static bool ParseDeclarationStatement(ReadOnlySpan<Token> tokens, out Result<Statement> result)
		{
			if (
				tokens.Length >= 1
				&& tokens[0] is { Kind: TokenKinds.KeyWord, Value: { KeyWord: KeyWords.Let } }
				&& ParseInBracketsOptional<Declaration>(tokens[1..], out var declarationResult, ParseDeclaration, Brackets.OpenExpression, Brackets.CloseExpression)
				&& declarationResult.Tokens[0] is { Kind: TokenKinds.Operator, Value: { Operator: Operators.Initialize } }
				&& ParseExpression(declarationResult.Tokens[1..], out var expressionResult)
			)
			{
				result = new Result<Statement>(
					new DeclarationStatement(declarationResult.Value, expressionResult.Value),
					expressionResult.Tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		#endregion
		#region identifiers

		private static bool ParseIdentifier(ReadOnlySpan<Token> tokens, out Result<Identifier> result)
		{
			if (
				tokens.Length >= 1
				&& tokens[0] is { Kind: TokenKinds.Identifier, Value: { Identifier: var identifier } }
			)
			{
				result = new Result<Identifier>(
					new Identifier(identifier),
					tokens[1..]
				);
				return true;
			}

			result = default;
			return false;
		}

		// private static bool ParseIdentifierPath(ReadOnlySpan<Token> tokens, out Result<Identifier[]> result)
		// {
		// 	if (ParseIdentifier(tokens, out var identifierResult))
		// 	{
		// 		tokens = identifierResult.Tokens;
		// 		using var path = TempList<Identifier>.Get();
		// 		path.Add(identifierResult.Value);
		// 		while (
		// 			tokens.Length >= 1
		// 			&& tokens[0] is { Kind: TokenKinds.Operator, Value: { Operator: Operators.SubModule } }
		// 			&& ParseIdentifier(tokens[1..], out var subIdentifierResult)
		// 		)
		// 		{
		// 			tokens = subIdentifierResult.Tokens;
		// 			path.Add(subIdentifierResult.Value);
		// 		}

		// 		result = new Result<Identifier[]>(
		// 			path.ToArray(),
		// 			tokens
		// 		);
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		// private static bool ParseIdentifierWithGenerics(ReadOnlySpan<Token> tokens, out Result<(Identifier Identifier, Identifier[] Generics)> result)
		// {
		// 	if (ParseIdentifier(tokens, out var identifierResult))
		// 	{
		// 		tokens = identifierResult.Tokens;
		// 		if (ParseInBrackets<Identifier>(tokens, out var genericIdentifierResult, ParseIdentifier, Brackets.OpenType, Brackets.CloseType))
		// 			result = new Result<(Identifier, Identifier[])>((identifierResult.Value, genericIdentifierResult.Value), genericIdentifierResult.Tokens);
		// 		else
		// 			result = new Result<(Identifier, Identifier[])>((identifierResult.Value, Array.Empty<Identifier>()), tokens);
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		// private static bool ParseIdentifierPathWithGenerics(ReadOnlySpan<Token> tokens, out Result<(Identifier[] Path, IdentifiedOr<Generic>[] Generics)> result)
		// {
		// 	if (ParseIdentifierPath(tokens, out var identifierResult))
		// 	{
		// 		tokens = identifierResult.Tokens;
		// 		if (ParseGenerics(tokens, out var genericsResult))
		// 			result = new Result<(Identifier[] Identifier, IdentifiedOr<Generic>[] Generics)>((identifierResult.Value, genericsResult.Value), genericsResult.Tokens);
		// 		else
		// 			result = new Result<(Identifier[] Identifier, IdentifiedOr<Generic>[] Generics)>((identifierResult.Value, Array.Empty<IdentifiedOr<Generic>>()), tokens);
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		#endregion
		#region Types

		private static bool ParseType(ReadOnlySpan<Token> tokens, out Result<Type> result)
		{
			if (tokens[0] is { Kind: TokenKinds.BaseType, Value: { BaseType: var baseType } })
			{
				tokens = tokens[1..];
				switch (baseType)
				{
					case BaseTypes.Module:
						result = new Result<Type>(new ModuleType(), tokens);
						return true;
					case BaseTypes.Type:
						result = new Result<Type>(new TypeType(), tokens);
						return true;

					case BaseTypes.Bool:
						result = new Result<Type>(new BoolType(), tokens);
						return true;
					case BaseTypes.Char:
						result = new Result<Type>(new CharType(), tokens);
						return true;

					case BaseTypes.Byte:
						result = new Result<Type>(new ByteType(), tokens);
						return true;
					case BaseTypes.Short:
						result = new Result<Type>(new ShortType(), tokens);
						return true;
					case BaseTypes.Int:
						result = new Result<Type>(new IntType(), tokens);
						return true;
					case BaseTypes.Long:
						result = new Result<Type>(new LongType(), tokens);
						return true;

					case BaseTypes.UByte:
						result = new Result<Type>(new UByteType(), tokens);
						return true;
					case BaseTypes.UShort:
						result = new Result<Type>(new UShortType(), tokens);
						return true;
					case BaseTypes.UInt:
						result = new Result<Type>(new UIntType(), tokens);
						return true;
					case BaseTypes.ULong:
						result = new Result<Type>(new ULongType(), tokens);
						return true;

					case BaseTypes.Float:
						result = new Result<Type>(new FloatType(), tokens);
						return true;
					case BaseTypes.Double:
						result = new Result<Type>(new DoubleType(), tokens);
						return true;

					case BaseTypes.Ref or BaseTypes.Array:
						if (ParseExpression(tokens, out var typeResult))
						{
							result = new Result<Type>(
								baseType is BaseTypes.Ref ? new RefType(typeResult.Value) : new ArrayType(typeResult.Value),
								typeResult.Tokens
							);
							return true;
						}
						break;

						// case BaseTypes.Array:
						// 	result = new Result<Type>(new ExplicitType(new ArrayType()), tokens[1..]);
						// 	return true;
						// case BaseTypes.Func:
						// 	result = new Result<Type>(new ExplicitType(new FuncType()), tokens[1..]);
						// 	return true;

						// case BaseTypes.Str:
						// 	result = new Result<Type>(new ExplicitType(new StrType()), tokens[1..]);
						// 	return true;
						// case BaseTypes.Slice:
						// 	result = new Result<Type>(new ExplicitType(new SliceType()), tokens[1..]);
						// 	return true;
						// case BaseTypes.Range:
						// 	result = new Result<Type>(new ExplicitType(new RangeType()), tokens[1..]);
						// 	return true;
						// case BaseTypes.Typeinfo:
						// 	result = new Result<Type>(new ExplicitType(new TypeinfoType(tokens[1..]);
						// 	return true;
						// case BaseTypes.Funcinfo:
						// 	result = new Result<Type>(new ExplicitType(new FuncinfoType(tokens[1..]);
						// 	return true;

						// case BaseTypes.Block:
						// 	if (
						// 		tokens.Length >= 1
						// 		&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.OpenType } }
						// 	)
						// 	{
						// 		tokens = tokens[1..];
						// 		if (
						// 			ParseIdentifiedOrValue(tokens, out var valueResult)
						// 			&& ParseTypeOrIdentified(valueResult.Tokens, out var typeResult)
						// 		)
						// 		{
						// 			tokens = typeResult.Tokens;
						// 			if (
						// 				tokens.Length >= 1
						// 				&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.CloseType } }
						// 			)
						// 			{
						// 				result = new Result<Type>(
						// 					new BlockType(valueResult.Value, typeResult.Value),
						// 					tokens[1..]
						// 				);
						// 				return true;
						// 			}
						// 		}
						// 	}
						// 	break;

						// case BaseTypes.Record or BaseTypes.Union:
						// 	if (
						// 		tokens.Length >= 1
						// 		&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.OpenType } }
						// 	)
						// 	{
						// 		tokens = tokens[1..];
						// 		using var fields = TempList<Field>.Get();
						// 		while (
						// 			ParseIdentifier(tokens, out var identifierResult)
						// 			&& ParseTypeOrIdentified(identifierResult.Tokens, out var typeResult)
						// 		)
						// 		{
						// 			tokens = typeResult.Tokens;
						// 			fields.Add(new Field(identifierResult.Value, typeResult.Value));

						// 			if (
						// 				tokens.Length >= 1
						// 				&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.CloseType } }
						// 			)
						// 			{
						// 				result = new Result<Type>(
						// 					baseType == BaseTypes.Record ? new RecordType(fields.ToArray()) : new UnionType(fields.ToArray()),
						// 					tokens[1..]
						// 				);
						// 				return true;
						// 			}
						// 		}
						// 	}
						// 	break;

						// case BaseTypes.Enum:
						// 	if (
						// 		tokens.Length >= 1
						// 		&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.OpenType } }
						// 	)
						// 	{
						// 		tokens = tokens[1..];
						// 		if (ParseTypeOrIdentified(tokens, out var typeResult))
						// 		{
						// 			tokens = typeResult.Tokens;
						// 			using var elements = TempList<EnumElement>.Get();
						// 			while (
						// 				ParseIdentifier(tokens, out var identifierResult)
						// 				&& ParseIdentifiedOrValue(identifierResult.Tokens, out var valueResult)
						// 			)
						// 			{
						// 				tokens = valueResult.Tokens;
						// 				elements.Add(new EnumElement(identifierResult.Value, valueResult.Value));
						// 				if (
						// 					tokens.Length >= 1
						// 					&& tokens[0] is { Kind: TokenKinds.Bracket, Value: { Bracket: Brackets.CloseType } }
						// 				)
						// 				{
						// 					result = new Result<Type>(
						// 						new EnumType(typeResult.Value, elements.ToArray()),
						// 						tokens[1..]
						// 					);
						// 					return true;
						// 				}
						// 			}
						// 		}
						// 	}
						// 	break;
				}
			}

			result = default;
			return false;
		}

		// private static bool ParseTypeOrIdentified(in ReadOnlySpan<Token> tokens, out Result<IdentifiedOr<Type>> result)
		// {
		// 	if (ParseType(tokens, out var typeResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Type>>(
		// 			new Concrete<Type>(typeResult.Value),
		// 			typeResult.Tokens
		// 		);
		// 		return true;
		// 	}
		// 	else if (ParseIdentifierPathWithGenerics(tokens, out var pathResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Type>>(
		// 			new Identified<Type>(pathResult.Value.Path, pathResult.Value.Generics),
		// 			pathResult.Tokens
		// 		);
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		#endregion
		#region Value

		private static bool ParseValue(ReadOnlySpan<Token> tokens, out Result<Value> result)
		{
			if (tokens.Length >= 1)
			{
				switch (tokens[0].Kind)
				{
					case TokenKinds.Bool:
						result = new Result<Value>(
							new BoolValue(tokens[0].Value.Bool),
							tokens[1..]
						);
						return true;
					case TokenKinds.Int:
						result = new Result<Value>(
							new IntValue(tokens[0].Value.Int),
							tokens[1..]
						);
						return true;
					case TokenKinds.UInt:
						result = new Result<Value>(
							new UIntValue(tokens[0].Value.UInt),
							tokens[1..]
						);
						return true;
					case TokenKinds.Float:
						result = new Result<Value>(
							new FloatValue(tokens[0].Value.Float),
							tokens[1..]
						);
						return true;
					case TokenKinds.String:
						result = new Result<Value>(
							new StringValue(tokens[0].Value.String),
							tokens[1..]
						);
						return true;
					default: break;
				}
			}

			if (ParseType(tokens, out var typeResult))
			{
				result = new Result<Value>(
					new TypeValue(typeResult.Value),
					typeResult.Tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		// private static bool ParseIdentifiedOrValue(ReadOnlySpan<Token> tokens, out Result<IdentifiedOr<Value>> result)
		// {
		// 	if (ParseValue(tokens, out var valueResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Value>>(
		// 			new Concrete<Value>(valueResult.Value),
		// 			valueResult.Tokens
		// 		);
		// 		return true;
		// 	}
		// 	else if (ParseIdentifierPathWithGenerics(tokens, out var identifierResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Value>>(
		// 			   new Identified<Value>(identifierResult.Value.Path, identifierResult.Value.Generics),
		// 			   identifierResult.Tokens
		// 		   );
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		#endregion
		#region function

		// private static bool ParseFunction(ReadOnlySpan<Token> tokens, out Result<Function> result)
		// {
		// 	if (
		// 		ParseInBrackets<Parameter>(tokens, out var inputParameters, ParseParameter, Brackets.OpenExpression, Brackets.CloseExpression)
		// 		&& ParseInBrackets<Parameter>(inputParameters.Tokens, out var outputParameters, ParseParameter, Brackets.OpenExpression, Brackets.CloseExpression)
		// 	)
		// 	{
		// 		tokens = outputParameters.Tokens;
		// 		if (
		// 			tokens.Length >= 2
		// 			&& tokens[0] is { Kind: TokenKinds.KeyWord, Value: { KeyWord: KeyWords.Extern } }
		// 			&& tokens[1] is { Kind: TokenKinds.String, Value: { String: var convention } }
		// 		)
		// 		{
		// 			result = new Result<Function>(
		// 				new ExternFunction(inputParameters.Value, outputParameters.Value, convention),
		// 				tokens[2..]
		// 			);
		// 			return true;
		// 		}
		// 		else if (ParseScope(tokens, out var scopeResult))
		// 		{
		// 			result = new Result<Function>(
		// 				new ImplementedFunction(inputParameters.Value, outputParameters.Value, scopeResult.Value),
		// 				scopeResult.Tokens
		// 			);
		// 			return true;
		// 		}
		// 	}

		// 	result = default;
		// 	return false;
		// }

		// private static bool ParseFunctionOrIdentified(in ReadOnlySpan<Token> tokens, out Result<IdentifiedOr<Function>> result)
		// {
		// 	if (ParseFunction(tokens, out var functionResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Function>>(
		// 			new Concrete<Function>(functionResult.Value),
		// 			functionResult.Tokens
		// 		);
		// 		return true;
		// 	}
		// 	else if (ParseIdentifierPathWithGenerics(tokens, out var identifierResult))
		// 	{
		// 		result = new Result<IdentifiedOr<Function>>(
		// 			   new Identified<Function>(identifierResult.Value.Path, identifierResult.Value.Generics),
		// 			   identifierResult.Tokens
		// 		   );
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		// private static bool ParseParameter(ReadOnlySpan<Token> tokens, out Result<Parameter> result)
		// {
		// 	if (
		// 		ParseIdentifier(tokens, out var identifierResult)
		// 		&& ParseTypeOrIdentified(identifierResult.Tokens, out var typeResult)
		// 	)
		// 	{
		// 		result = new Result<Parameter>(
		// 			new Parameter(identifierResult.Value, typeResult.Value),
		// 			typeResult.Tokens
		// 		);
		// 		return true;
		// 	}

		// 	result = default;
		// 	return false;
		// }

		private static bool ParseScope(ReadOnlySpan<Token> tokens, out Result<Scope> result)
		{
			if (ParseInBrackets<Statement>(tokens, out var statementsResult, ParseStatement, Brackets.OpenScope, Brackets.CloseScope))
			{
				result = new Result<Scope>(
					new Scope(statementsResult.Value),
					statementsResult.Tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		#endregion
		#region expression

		private static bool ParseAtomicExpression(ReadOnlySpan<Token> tokens, out Result<Expression> result)
		{
			if (ParseInBrackets<Expression>(tokens, out var expressionsResult, ParseExpression, Brackets.OpenExpression, Brackets.CloseExpression))
			{
				result = new Result<Expression>(
					new MultiExpression(expressionsResult.Value),
					expressionsResult.Tokens
				);
				return true;
			}
			else if (ParseValue(tokens, out var valueResult))
			{
				result = new Result<Expression>(
					new ValueExpression(valueResult.Value),
					valueResult.Tokens
				);
				return true;
			}
			// else if (
			// 	ParseFunctionOrIdentified(tokens, out var functionResult)
			// 	&& ParseInBrackets<Expression>(functionResult.Tokens, out var parametersResult, ParseExpression, Brackets.OpenExpression, Brackets.CloseExpression)
			// )
			// {
			// 	result = new Result<Expression>(
			// 		new FunctionCallExpression(functionResult.Value, parametersResult.Value),
			// 		parametersResult.Tokens
			// 	);
			// 	return true;
			// }

			result = default;
			return false;
		}

		private static bool ParseExpression(ReadOnlySpan<Token> tokens, out Result<Expression> result)
		{
			if (ParseAtomicExpression(tokens, out var lhs))
			{
				tokens = lhs.Tokens;

				using var expressions = TempList<Expression>.Get();
				expressions.Add(lhs.Value);
				using var operators = TempList<Operators>.Get();
				while (
					tokens.Length >= 1
					&& tokens[0] is { Kind: TokenKinds.Operator, Value: { Operator: var @operator } }
					&& @operator is not Operators.Initialize or Operators.Assign
					&& ParseAtomicExpression(tokens[1..], out var rhs)
				)
				{
					tokens = rhs.Tokens;
					expressions.Add(rhs.Value);
					operators.Add(@operator);
				}

				var expression = expressions[0];

				for (var i = 1; i < expressions.Count; i++)
					expression = new OperatorExpression(expression, expressions[i], (Ast.Operators)operators[i - 1]);

				result = new Result<Expression>(
					expression,
					tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		#endregion
		#region declarations

		private static bool ParseDeclaration(ReadOnlySpan<Token> tokens, out Result<Declaration> result)
		{
			var isMut = false;
			if (tokens[0] is { Kind: TokenKinds.KeyWord, Value: { KeyWord: KeyWords.Mut } })
			{
				tokens = tokens[1..];
				isMut = true;
			}

			if (
				ParseIdentifier(tokens, out var identifierResult)
				&& ParseExpression(identifierResult.Tokens, out var expressionResult)
			)
			{
				result = new Result<Declaration>(
					new Declaration(isMut, identifierResult.Value, expressionResult.Value),
					expressionResult.Tokens
				);
				return true;
			}

			result = default;
			return false;
		}

		#endregion
		#region in brackets

		private delegate bool ParserDelegate<T>(ReadOnlySpan<Token> tokens, out Result<T> result);

		private static bool ParseInBrackets<T>(ReadOnlySpan<Token> tokens, out Result<T[]> result, ParserDelegate<T> elementParser, Brackets open, Brackets close)
		{
			if (
				tokens.Length >= 1
				&& tokens[0] is { Kind: TokenKinds.Bracket }
				&& tokens[0].Value.Bracket == open
			)
			{
				tokens = tokens[1..];
				using var elements = TempList<T>.Get();
				while (elementParser(tokens, out var elementResult))
				{
					tokens = elementResult.Tokens;
					elements.Add(elementResult.Value);
				}

				if (
					tokens.Length >= 1
					&& tokens[0] is { Kind: TokenKinds.Bracket }
					&& tokens[0].Value.Bracket == close
				)
				{
					result = new Result<T[]>(
						elements.ToArray(),
						tokens[1..]
					);
					return true;
				}
			}

			result = default;
			return false;
		}

		private static bool ParseInBracketsOptional<T>(ReadOnlySpan<Token> tokens, out Result<T[]> result, ParserDelegate<T> elementParser, Brackets open, Brackets close)
		{
			if (ParseInBrackets(tokens, out result, elementParser, open, close))
			{
				return true;
			}
			else if (elementParser(tokens, out var valueResult))
			{
				result = new Result<T[]>(
					new T[] { valueResult.Value },
					valueResult.Tokens
				);
				return true;
			}
			result = default;
			return false;
		}

		#endregion

		public readonly ref struct Result<T>
		{
			public readonly T Value;
			public readonly ReadOnlySpan<Token> Tokens;

			public Result(T value, ReadOnlySpan<Token> tokens)
			{
				Value = value;
				Tokens = tokens;
			}
		}
	}

	public readonly struct ParseResult
	{
		public readonly Scope Scope;
		public readonly ParseError[] Errors;

		public ParseResult(Scope scope, ParseError[] errors)
		{
			Scope = scope;
			Errors = errors;
		}

		public override string ToString() => Scope.ToString() + "\n" + Errors.ToStringArrayScope(true);
	}

	public readonly struct ParseError
	{
		public readonly Token Token;

		public ParseError(Token token)
		{
			Token = token;
		}

		public override string ToString() => Token.ToString();
	}
}
