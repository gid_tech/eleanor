_ :: mod "test"
bar :: mod "test/foo/bar"

// elementary types
MyBool :: bool
MyFloat :: float

// aggregate types
MyBlock :: block 16 float
MyRecord :: struct {a : float, b : float}
MyUnion :: union {a : double, b : long}

// ptr types
MyRef :: ref MyRecord
MyArray :: array MyRecord
MyFunc :: proc (float, int) to (double, long) // function ptr

// fat types
MyStr :: str
MySlice :: slice int
MyRange :: range
MyType :: type // reflection types?

// enum types
MyEnum :: enum short {
	A : 0
	B : 1
	C : 2
}

// complex
MyRecord2 :: struct {
	a : float
	b : int
}
MyRecordEnum : enum MyRecord {
	X : default
	Y : MyRecord {3, 4}
}

// value defines
MyVal :: float 1.1
MySVal :: MyRecord [a = 3, b = 4]
MyAVal :: MyArray [1, 2, 3, #5 = 5, ..default]
MyString :: str "hello"

// globals
my_global := uint 1 // global
my_exported #export := uint 1 // exported global

(ga, gb) :: (1, 2) // multi value?

// funcs
MyProc :: func (a : int) to (b : int) {
	b = a
}


MyProc :: func (a float) to (b float) {
	//local varaible
	c := float a
	
	// assignment
	b = add(1, 2)

	// local decl with anonymous record type
	baz :: record [v int, b uint] default

	(baz.v, baz.b) = swap(1, 2)

	// local func
	add :: func a float, b float => c float {
		c = a + b
	}

	// multi return
	swap :: func (a float, b float) to (c float, d float) {
		(c, d) = (b, a)
	}
	(a, b) := swap(1, 2)

	// condition
	if add(1, 2) > 3 then {	}
	// loop
	while baz < 10 do {baz <- baz + 1}

	a := {
		aa := 1
		aa
	}
}

// generics

// value
GenValue :: func [T : type] (a : T) to (b : T) {
	b = a
}

// type 
GenType :: record [T : type] {
	val : T
}

// func
GenFunc :: func[T type] () to () {}

Block :: block [T : type, C : ulong] C T

// list test

func malloc (size ulong) -> (array byte) "C" = import "malloc"
func realloc (size ulong, let p array byte) -> (array byte) "C" = import "realloc"
func free () -> (array byte) "C" = import "free"

type List[T type] = record [
	elements array T
	len ulong
]

func list_add[T type] (list ref List[T], element T) -> () = {
	list!.len <- list!.len + 1
	
	list!.elements <- realloc(
		list!.elements ~ array byte
		list!.len * T.size
	) ~ array T

	list!.elements#(list.len - 1) <- element
}

func list_deinit[T type] : (list List[T]) -> () = {
	free[list.elements ~ array byte]
}

func list_len[T type] : (list ref List[T]) -> ulong = {
	return list!.len
}

global_list List:double = default

func main () -> () = {
	let a Block[int, 3] = [1, 2, 3]

    mut list List[float] = default
	defer list_deinit:float list
	
	list_add(list?, 1)
	list_add(list?, 3)
	list_add(list?, 3)
	
	list.elements#0 <- 5

	len ulong = list_len(list?)

	scope {
		list_add(global_list?, 1)
		global_list Deinit
	}
}


Foo : struct () -> (a : int) = {
	a = 2
	defer a = 0
}

foo : func () -> (a : int) = {
	a = 2
	defer a = 0
}

